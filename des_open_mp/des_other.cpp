#include <fstream>

#include "des_other.h"

uint64 rb(const char* filename, char** content)
{
	std::ifstream ifile;
	ifile.open(filename, std::ios::in | std::ios::binary | std::ios::ate);

	uint64 size = ifile.tellg();
	ifile.seekg(0, std::ios::beg);

	*content = (char*)malloc(size);

	if (!*content)
		return 0;

	ifile.read(*content, size);

	return size;
}

void wb(const char* filename, const char* content, uint64 size)
{
	std::ofstream ofile;
	ofile.open(filename, std::ios::out | std::ios::binary | std::ios::trunc);

	ofile.write(content, size);
}


uint64 rseek_end(const char* content, uint64 from)
{
	const char* last = content + from - 1;

	while (last >= content && *last == '\0')
		last--;

	return (uint64)(last - content) + 1;
}