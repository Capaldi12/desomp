#include <iostream>
#include <chrono>

#include <omp.h>

#include "des.h"
#include "des_key.h"
#include "des_lookup.h"
#include "des_other.h"

// ������� �����
constexpr uint32 take_1_bit = 0x01;  // ������� ���
constexpr uint32 take_4_bits = 0x0f;  // ������� 4 ����
constexpr uint32 take_28_bits = 0x0fffffff;  // ������� 28 ���
// constexpr uint32 take_32_bits = 0xffffffff;  // ������� 32 ����

// ����� ��� ������������� ������ ����������� (S-box)
constexpr uint64 outer = 0x0000840000000000;  // ������� 2 ���� [...00010000100...]
constexpr uint64 inner = 0x0000780000000000;  // ���������� 4 ���� [...00001111000...]


// ����� ������� 32 ���� 64-������� �����
#define high_32(a) ((a >> 32) & 0xffffffff)

// ����� ������� 32 ���� 64-������� �����
#define low_32(a) (a & 0xffffffff)

// ����� ������� 28 ����� 56-������� �����
#define high_28(a) ((a >> 28) & 0x0fffffff)

// ����� ������� 28 ����� 56-������� �����
#define low_28(a) (a & 0x0fffffff)

// ����������� ����� ����� ��� 28-������� �����
#define rotl_28(a) ((a << 1) & take_28_bits) | ((a >> 27) & take_1_bit)


uint64 permute(uint64 seq, uint8 src_len, uint8 dst_len, const char* lookup)
{
	uint64 result = 0;

	for (uint8 i = 0; i < dst_len; i++)
	{
		result <<= 1;
		result |= (seq >> (src_len - lookup[i])) & take_1_bit;
	}

	return result;
}

// ��������� ��������� ������
void key_schedule(uint64 key) 
{
	// �������� ������������
	key = permute(key, 64, 56, PERM1);

	// ���������� ����� �� ��� 28-������ �����
	uint32 C = (uint32) high_28(key);
	uint32 D = (uint32) low_28(key);

	// ��������� ��������� ������
	for (uint8 i = 0; i < 16; i++)
	{
		// ��������� ����� ������
		for (uint8 j = 0; j < ROUND_SHIFT[i]; j++)
		{
			C = rotl_28(C);
			D = rotl_28(D);
		}

		// ������������
		key = ((uint64)C << 28) | (uint64)D;

		// ��������� ������������
		round_key[i] = permute(key, 56, 48, PERM2);
	}
}

uint64 des_block(uint64 block, bool mode)
{
	// ��������� ������������
	block = permute(block, 64, 64, IP);

	uint32 L = (uint32)high_32(block);
	uint32 R = (uint32)low_32(block);

	// 16 ������� ���� ��������
	for (uint8 i = 0; i < 16; i++)
		feistel(L, R, mode ? round_key[15 - i] : round_key[i]);

	// �������������� ����� � ��������� �������� �������
	block = ((uint64)R << 32) | (uint64)L;

	// �������� ������������
	return permute(block, 64, 64, IIP);
}

void feistel(uint32& L, uint32& R, uint64 round_key)
{
	// ����������
	uint64 r_in = permute((uint64)R, 32, 48, EXP);

	// f(R, k) => R XOR k
	r_in ^= round_key;

	uint32 r_out = 0;

	// ���������� ������ ����������� (S-box)
	for (uint8 i = 0; i < 8; i++)
	{
		// ������� ���� - ����� ������
		uint8 row = (uint8)((r_in & (outer >> 6 * i)) >> (42 - 6 * i));
		row = (row >> 4) | (row & take_1_bit);

		// ���������� ���� - ����� �������
		uint8 col = (uint8)((r_in & (inner >> 6 * i)) >> (42 - 6 * i));

		r_out <<= 4;
		r_out |= (uint32)(SBOX[i][16 * row + col] & take_4_bits);
	}

	// ��������� ������������
	r_out = (uint32)permute((uint64)r_out, 32, 32, P);

	// ���������� xor � ������������ ������
	uint32 temp = R;
	R = L ^ r_out;  // L XOR f(R, k)
	L = temp;
}

uint64 des(uchar* payload, uint64 size, uchar** output, bool mode)
{
	// ����������� �������
	uint64 out_size = size / 8 + 1;

	if (mode)
		out_size--;

	// ��������� ������
	*output = (uchar*)calloc(out_size, sizeof(uint64));

	if (!*output)
		return 0;

	// ����������� � ��������� ������
	memcpy(*output, payload, size);

	// �������������� ���������
	uint64* block = (uint64*)*output;

	// ��������� ����������/������������
	for (uint64 i = 0; i < out_size; i++)
		block[i] = des_block(block[i], mode);
	
	// ����������� ����� ����������
	return mode ? rseek_end((char*)*output, out_size * 8) : out_size * 8;
}

// ���������� �� ���������� ������ �������� ��������� ��� OpenMP
uint64 des_parallel(uchar* payload, uint64 size, uchar** output, bool mode)
{
	// ����������� �������
	uint64 out_size = size / 8 + 1;

	if (mode)
		out_size--;

	// ��������� ������
	*output = (uchar*)calloc(out_size, sizeof(uint64));

	if (!*output)
		return 0;

	// ����������� � ��������� ������
	memcpy(*output, payload, size);

	uint64* block = (uint64*)*output;

	// ��������� ����������/������������
	#pragma omp parallel for
	for (long long i = 0; i < out_size; i++)
		block[i] = des_block(block[i], mode);

	// ����������� ����� ����������
	return mode ? rseek_end((char*)*output, out_size * 8) : out_size * 8;
}

using namespace std::chrono;

int des_file(const char* input, const char* output, bool mode, bool parallel, bool timeit)
{
	// ������ �����
	char* buffer = nullptr;
	int size = rb(input, &buffer);

	if (!size)
		return 1;

	uint64(*des_func)(uchar*, uint64, uchar**, bool);

	// ������� ������ ������
	if (parallel)
		des_func = &des_parallel;
	else
		des_func = &des;

	// ��������� ����������
	uchar* result = nullptr;

	auto start = high_resolution_clock::now();

	int len = des_func((uchar*)buffer, size, &result, mode);

	auto end = high_resolution_clock::now();

	free(buffer);
	buffer = nullptr;

	// ���������� ���������� � ����
	if (len)
	{
		wb(output, (char*)result, len);
		free(result);
	}
	else
		return 1;

	// ����� ������� ����������
	if (timeit)
	{
		auto elapsed = duration_cast<microseconds>(end - start);

		std::cout << "Elapsed time: " << elapsed.count() << " mcs" << std::endl;
	}

	return 0;
}