#ifndef DES
#define DES

#include <cstdint>

#define uint64 std::uint64_t
#define uint32 std::uint32_t
#define uint8 std::uint8_t

#define uchar unsigned char

// 48-������ ��������� �����
static uint64 round_key[16]; 

/// <summary>
/// �������� ����������� �� ������ ������� ������.
/// </summary>
/// <param name="seq">�������� ������������������.</param>
/// <param name="src_len">����� ������������.</param>
/// <param name="dst_len">����� ������������.</param>
/// <param name="lookup">������� ������.</param>
/// <returns>�������������� ������������.</returns>
uint64 permute(uint64 seq, uint8 src_len, uint8 dst_len, const char* lookup);

/// <summary>
/// ���������� ������ - ��������� ��������� ������ ��� ����������.
/// <para>�</para>
/// 
/// ����� ������������ � ������ `round_key`.
/// </summary>
/// <param name="key">�������� ����</param>
void key_schedule(uint64 key);

/// <summary>
/// �������� �������� DES. 
/// <para>�</para>
/// <para>������� ��� ��������� ���� � �������������� ������ � ������� `round_key`.</para>
/// <para>����� ������ ������� �� �������� ��������� `mode`: 
/// false - ����������, true - �����������.</para>
/// <para>�� ������ ������ ������ `round_key` ������ ���� ��������������� �������� `key_schedule`.</para>
/// </summary>
/// <param name="block">���� ��� ���������.</param>
/// <param name="mode">����� ������.</param>
/// <returns>������������ ����.</returns>
uint64 des_block(uint64 block, bool mode);

/// <summary>
/// ����� ���� ��������.
/// </summary>
/// <param name="L">������� �������� �����.</param>
/// <param name="R">������� �������� �����.</param>
/// <param name="round_key">��������� ����.</param>
void feistel(uint32 &L, uint32 &R, uint64 round_key);

/// <summary>
/// ������� ��� ��������� ���������.
/// <para>�</para>
/// <para>������������ �������� �������� ����� ����������. 0 ������������� ������.</para>
/// <para>���� ������������ �������� ������������� `output` �������� ��������� �� ���������.</para>
/// <para>������ ��� ��������� �������� ����������� � ������ ���� ����������� ���������� ��������.</para>
/// <para>`mode` ���������� ����� ������: false - ����������, true - ����������.</para>
/// </summary>
/// <param name="payload">���������/����������.</param>
/// <param name="size">����� ���������/�����������.</param>
/// <param name="output">��������� ������.</param>
/// <param name="mode">����� ������.</param>
/// <returns>����� ����������.</returns>
uint64 des(uchar* payload, uint64 size, uchar** output, bool mode);

/// <summary>
/// ������� ��� ��������� ���������, ���������� OpenMP ��� �����������������.
/// <para>�</para>
/// <para>������������ �������� �������� ����� ����������. 0 ������������� ������.</para>
/// <para>���� ������������ �������� ������������� `output` �������� ��������� �� ���������.</para>
/// <para>������ ��� ��������� �������� ����������� � ������ ���� ����������� ���������� ��������.</para>
/// <para>`mode` ���������� ����� ������: false - ����������, true - ����������.</para>
/// </summary>
/// <param name="payload">���������/����������.</param>
/// <param name="size">����� ���������/�����������.</param>
/// <param name="output">��������� ������.</param>
/// <param name="mode">����� ������.</param>
/// <returns>����� ����������.</returns>
uint64 des_parallel(uchar* payload, uint64 size, uchar** output, bool mode);

/// <summary>
/// ������� ��� ��������� ���������� �����. 
/// </summary>
/// <param name="input">�������� ��������� �����.</param>
/// <param name="output">�������� ��������������� �����.</param>
/// <param name="mode">����� ������.</param>
/// <param name="parallel">������������ ���������������� ������ ���������</param>
/// <param name="timeit">�������� ����� ����������</param>
/// <returns>0 � ������ ������.</returns>
int des_file(const char* input, const char* output, bool mode, bool parallel = false, bool timeit = false);

#endif // !DES
