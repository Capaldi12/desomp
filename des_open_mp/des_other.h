#ifndef DES_OTHER
#define DES_OTHER

#include "des.h"

/// <summary>
/// ������ ����� � �������� ������.
/// </summary>
/// <param name="filename">��� �����.</param>
/// <param name="content">��������� ��� ����������� �����.</param>
/// <returns>����� �����.</returns>
uint64 rb(const char* filename, char** content);

/// <summary>
/// ������ ����� � �������� ������.
/// </summary>
/// <param name="filename">��� �����.</param>
/// <param name="content">���������� �����.</param>
/// <param name="size">����� �����.</param>
void wb(const char* filename, const char* content, uint64 size);

/// <summary>
/// ����� ��������� ��������� ������ � ������� (����� �����).
/// </summary>
/// <param name="content">������.</param>
/// <param name="from">����� �������.</param>
/// <returns></returns>
uint64 rseek_end(const char* content, uint64 from);

#endif // !DES_OTHER

