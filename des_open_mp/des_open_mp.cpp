#include <iostream>
#include <random>

#include "cxxopt.hpp"

#include "des.h"
#include "des_other.h"

#define print(stuff) std::cout << stuff << std::endl

void send_help(cxxopts::Options opts)
{
    print(opts.help({ "command", "file", "other" }));
}

void keygen(const char* filename)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());

    uint64 key = gen();

    wb(filename, (char*)&key, 8);
}

uint64 read_key(const char* filename)
{
    char* content;
    uint64 key = 0;

    int len = rb(filename, &content);

    if (len)
    {
        if (len == 8)
            key = *((uint64*)content);

        free(content);
    }

    return key;
}

void process(const char* input, const char* output, uint64 key, bool mode, bool parallel = false, bool timeit = false)
{
    key_schedule(key);
    int res = des_file(input, output, mode, parallel, timeit);

    if (res)
        std::cout << "Error during " << (mode ? "decryption" : "encryption") << " occured." << std::endl;
}

int main(int argc, char* argv[])
{
    // ��������� ��������� ������
    cxxopts::Options options(argv[0], "Performs DES encryption using OpenMP for parallelizing.");
    options.custom_help("[command] [options]");

    options.add_options("command")
        ("keygen", "Generate key. Requires key file specified.")
        ("encrypt", "Encrypt file. Requires input, output and key files specified.")
        ("decrypt", "Decrypt file. Requires input, output and key files specified.");
        
    options.add_options("file")
        ("i,input", "Input file", cxxopts::value<std::string>())
        ("o,output", "Output file", cxxopts::value<std::string>())
        ("k,key", "Key file", cxxopts::value<std::string>());

    options.add_options("other")
        ("p,parallel", "Use OpenMP for parallelizing.")
        ("t,timeit", "Get program execution time.")
        ("h,help", "Print help message.");
    
    cxxopts::ParseResult result;

    try {
        result = options.parse(argc, argv);
    }
    catch (cxxopts::option_not_exists_exception e)
    {
        print(e.what());
        return 0;
    }

    
    if (result["help"].as<bool>())  // ��������������� ���������
    {
        send_help(options);
        return 0;
    }
    
    if (result["keygen"].as<bool>())  // ��������� �����
    {
        std::string filename;

        try 
        {
            filename = result["key"].as<std::string>();
        }

        catch (cxxopts::OptionException e)
        {
            print("Please specify file to save the key!");
            return 0;
        }

        keygen(filename.c_str());
        return 0;
    }

    bool enc = result["encrypt"].as<bool>();
    bool dec = result["decrypt"].as<bool>();
    bool timeit = result["timeit"].as<bool>();
    bool parallel = result["parallel"].as<bool>();

    if (enc || dec)  // ����������/�����������
    {
        std::string f_in;
        std::string f_out;
        std::string f_key;

        try
        {
            f_in = result["input"].as<std::string>();
        }
        catch (cxxopts::OptionException e)
        {
            print("Please specify input file!");
            return 0;
        }

        try
        {
            f_out = result["output"].as<std::string>();
        }
        catch (cxxopts::OptionException e)
        {
            print("Please specify output file!");
            return 0;
        }

        try
        {
            f_key = result["key"].as<std::string>();
        }
        catch (cxxopts::OptionException e)
        {
            print("Please specify key file!");
            return 0;
        }

        uint64 key = read_key(f_key.c_str());

        if (!key)
        {
            print("Invalid key!");
            return 0;
        }

        process(f_in.c_str(), f_out.c_str(), key, !enc, parallel, timeit);
    }
    else
    {
        print("Please specify arguments. Here's help message:\n");
        print(options.help());
    }

    return 0;
}
